# Code written by Oladeji Sanyaolu (Powerup) 11/2/2022

extends Sprite

export(String) var player_variable_modify : String
export(float) var player_variable_set : float
export(float) var powerup_cooldown : float = 2

onready var hitbox : Area2D = get_node("Hitbox")

func _ready() -> void:
	hitbox.connect("area_entered", self, "_on_Hitbox_area_entered")

func _on_Hitbox_area_entered(area : Area2D) -> void:
	if area.is_in_group("Player"):
		var area_parent : Sprite = area.get_parent()
		
		area_parent.set(player_variable_modify, player_variable_set)
		area_parent.get_node("Powerup_Cooldown").wait_time = 1
		area_parent.get_node("Powerup_Cooldown").start()
		area_parent.powerup_reset.append(name)
		queue_free()
	pass
