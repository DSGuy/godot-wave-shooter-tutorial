# Code written by Oladeji Sanyaolu (Global) 11/2/2022

extends Node

const ENCRYPT_KEY : String = "jt2AZa9$P%oKqE4*SVa%"

var node_creation_parent = null
var player = null
var camera = null

var points : int = 0
var highscore : int = 0


func instance_node(node : PackedScene, position : Vector2, parent : Node) -> Node:
	var node_instance : Node = node.instance()
	node_instance.global_position = position
	parent.add_child(node_instance)
	
	return node_instance

func save_data() -> Dictionary:
	var save_dict : Dictionary = {
		"highscore" : highscore,
		
	}
	
	return save_dict

func save_game() -> void:
	var save_game : File = File.new()
	save_game.open_encrypted_with_pass("user://savegame.save", File.WRITE, ENCRYPT_KEY)
	save_game.store_line(to_json(save_data()))
	save_game.close()

func load_game() -> void:
	var save_game : File = File.new()
	if not save_game.file_exists("user://savegame.save"):
		printerr("Error! We don't have a save file to load!")
		return
	
	save_game.open_encrypted_with_pass("user://savegame.save", File.READ, ENCRYPT_KEY)
	
	var current_line : Dictionary = parse_json(save_game.get_line())
	
	highscore = current_line["highscore"]
	
	save_game.close()
