extends Node2D

var fade : bool = false

var alpha : float = 1

onready var particles : CPUParticles2D = get_node("Particles")
onready var freezeBlood : Timer = get_node("Freeze_Blood")
onready var fadeoutTimer : Timer = get_node("Fadeout_Timer")

func _ready() -> void:
	# Particle Old colour: f04777
	freezeBlood.connect("timeout", self, "_on_Freeze_Blood_timeout")
	fadeoutTimer.connect("timeout", self, "_on_Fadeout_Timer_timeout")
	pass

func _process(_delta) -> void:
	if fade:
		alpha = lerp(alpha, 0, 0.05)
		modulate.a = alpha

		if alpha < 0.005:
			pass#queue_free()

func _on_Fadeout_Timer_timeout() -> void:
	fade = true
	pass

func _on_Freeze_Blood_timeout() -> void:
	particles.set_process(false)
	particles.set_physics_process(false)
	particles.set_process_input(false)
	particles.set_process_internal(false)
	particles.set_process_unhandled_input(false)
	particles.set_process_unhandled_key_input(false)
