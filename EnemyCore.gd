# Code written by Oladeji Sanyaolu (EnemyCore) 11/2/2022

extends Sprite

export(int) var speed : int = 75
export(int) var hp : int = 3
export(int) var knockback_strength : int = 600
export(int) var screen_shake : int = 120
export(int) var point_value : int = 10

var velocity : Vector2 = Vector2.ZERO

var stunned : bool = false
var blood_particles : PackedScene = preload("res://Blood_Particles.tscn")

onready var current_colour : Color = modulate

func basic_movement_towards_player(delta : float) -> void:
	if Global.player != null and !stunned:
		velocity = global_position.direction_to(Global.player.global_position)
		global_position += velocity * speed * delta
	elif stunned:
		velocity = lerp(velocity, Vector2.ZERO, 0.3)
		global_position += velocity * delta
	

func check_hp() -> void:
	if hp <= 0:
		if Global.camera != null:
			Global.camera.screen_shake(screen_shake, 0.2)
			
		Global.points += point_value
		if Global.node_creation_parent != null:
			var blood_particles_instance : Node2D = Global.instance_node(blood_particles, global_position, Global.node_creation_parent)
			blood_particles_instance.rotation = velocity.angle()
			blood_particles_instance.modulate = Color.from_hsv(current_colour.h, 0.5, current_colour.v)
		queue_free()

func hitbox_area_entered(area : Area2D, timer : Timer) -> void:
	if area.is_in_group("Enemy_Damager") and !stunned:
		modulate = Color.white
		velocity = -velocity * knockback_strength
		stunned = true
		hp -= area.get_parent().damage
		timer.start()
		area.get_parent().queue_free()
	pass

func stun_Timer_timeout() -> void:
	modulate = current_colour#Color("f04747")
	stunned = false
	pass
