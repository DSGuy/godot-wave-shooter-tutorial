# Code written by Oladeji Sanyaolu (Player) 11/2/2022

extends Sprite

const DEFAULT_RELOAD_SPEED : float = 0.1
const DEFAULT_DAMAGE : int = 1

var speed : int = 150
var damage : int = 1

var velocity : Vector2 = Vector2.ZERO
var game_vector : Vector2

var can_shoot : bool = true
var is_dead : bool = false

var reload_speed : float = 0.1

var bullet : PackedScene = preload("res://Bullet.tscn")

var powerup_reset : Array = []

onready var reloadSpeed = get_node("Reload_Speed")
onready var powerupCooldown = get_node("Powerup_Cooldown")
onready var hitbox = get_node("Hitbox")

func _ready() -> void:
	Global.player = self
	reloadSpeed.connect("timeout", self, "_on_Reload_Speed_timeout")
	powerupCooldown.connect("timeout", self, "_on_Powerup_Cooldown_timeout")
	hitbox.connect("area_entered", self, "_on_Hitbox_area_entered")
	hitbox.add_to_group("Player")

func _process(delta : float) -> void:
	game_vector.x = ProjectSettings.get_setting("display/window/size/width")
	game_vector.y = ProjectSettings.get_setting("display/window/size/height")
	
	velocity.x = int(Input.is_action_pressed("move_right")) - int(Input.is_action_pressed("move_left"))
	velocity.y = int(Input.is_action_pressed("move_down")) - int(Input.is_action_pressed("move_up"))
	
	velocity = velocity.normalized()
	
	global_position.x = clamp(global_position.x, 24, game_vector.x - 24)
	global_position.y = clamp(global_position.y, 24, game_vector.y - 24)
	if !is_dead:
		global_position += speed * velocity * delta
	
	if Input.is_action_pressed("click") and Global.node_creation_parent != null and can_shoot and !is_dead:
		var bullet_instance : Sprite = Global.instance_node(bullet, global_position, Global.node_creation_parent)
		bullet_instance.damage = damage
		
		reloadSpeed.start()
		can_shoot = false

func _on_Reload_Speed_timeout() -> void:
	can_shoot = true
	reloadSpeed.wait_time = reload_speed

func _on_Powerup_Cooldown_timeout() -> void:
	if powerup_reset.find("PowerupReload") != null:
		reload_speed = DEFAULT_RELOAD_SPEED
		powerup_reset.erase("PowerupReload")
	elif powerup_reset.find("PowerupDamage") != null:
		damage = DEFAULT_DAMAGE
		powerup_reset.erase("PowerupDamage")
	pass
	
func _on_Hitbox_area_entered(area : Area2D) -> void:
	if area.is_in_group("Enemy"):
		is_dead = true
		visible = false
		Global.save_game()
		yield(get_tree().create_timer(2), "timeout")
		get_tree().reload_current_scene()

func _exit_tree() -> void:
	Global.player = null
