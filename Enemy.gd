# Code written by Oladeji Sanyaolu (Enemy) 11/2/2022

extends "res://EnemyCore.gd"

onready var hitbox : Area2D = get_node("Hitbox")
onready var stunTimer : Timer = get_node("Stun_Timer")

func _ready() -> void:
	hitbox.add_to_group("Enemy")
	
	hitbox.connect("area_entered", self, "_on_Hitbox_area_entered")
	stunTimer.connect("timeout", self, "_on_Stun_Timer_timeout")

func _process(delta : float) -> void:
	basic_movement_towards_player(delta)
	check_hp()
	pass

func _on_Hitbox_area_entered(area : Area2D) -> void:
	hitbox_area_entered(area, stunTimer)
	pass

func _on_Stun_Timer_timeout() -> void:
	stun_Timer_timeout()
	pass
