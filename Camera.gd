# Code written by Oladeji Sanyaolu (Camera) 15/2/2022

extends Camera2D

var screen_shake_start : bool = false
var shake_intensity : int = 0

var intensity_percent : float = 0.0015
var vector_pos : Vector2 = Vector2(global_position.x, global_position.y)

onready var screenShakeTime = get_node("Screen_Shake_Time")

func _ready() -> void:
	Global.camera = self
	screenShakeTime.connect("timeout", self, "_on_Screen_Shake_Time_timeout")

func _process(delta : float) -> void:
	zoom = lerp(zoom, Vector2.ONE, 0.3)
	
	if screen_shake_start:
		global_position += Vector2(rand_range(-shake_intensity, shake_intensity), rand_range(-shake_intensity, shake_intensity)) * delta
	else:
		global_position = lerp(global_position, vector_pos, 0.3)

func screen_shake(intensity, time) -> void:
	zoom = Vector2.ONE - Vector2(intensity * intensity_percent, intensity * intensity_percent)
	shake_intensity = intensity
	screenShakeTime.wait_time = time
	screenShakeTime.start()
	screen_shake_start = true
	pass

func _on_Screen_Shake_Time_timeout() -> void:
	screen_shake_start = false

func _exit_tree() -> void:
	Global.camera = null
