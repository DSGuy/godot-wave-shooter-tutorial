# Code written by Oladeji Sanyaolu (Arena) 11/2/2022

extends Node2D

export(Array, PackedScene) var enemies : Array
export(Array, PackedScene) var powerups : Array

onready var enemySpawnTimer : Timer = get_node("Enemy_Spawn_Timer")
onready var difficultyTimer : Timer = get_node("Difficulty_Timer")
onready var powerupSpawnTimer : Timer = get_node("Powerup_Spawn_Timer")

onready var score : Label = get_node("UI").get_node("Control").get_node("Score")
onready var highscore : Label = get_node("UI").get_node("Control").get_node("Highscore")

func _ready() -> void:
	randomize()
	
	Global.node_creation_parent = self
	Global.points = 0
	
	Global.load_game()
	
	highscore.text = "Highscore: " + String(Global.highscore)
	
	enemySpawnTimer.connect("timeout", self, "_on_Enemy_Spawn_Timer_timeout")
	difficultyTimer.connect("timeout", self, "_on_Difficulty_Timer_timeout")
	powerupSpawnTimer.connect("timeout", self, "_on_Powerup_Spawn_Timer_timeout")

func _process(delta : float) -> void:
	score.text = String(Global.points)
	
	if Global.points > Global.highscore:
		Global.highscore = Global.points

func _on_Enemy_Spawn_Timer_timeout() -> void:
	var enemy_position : Vector2 = Vector2(rand_range(-160, 670), rand_range(-90, 390))
	var enemy_number : int = round(rand_range(0, enemies.size() - 1))
	
	while (enemy_position.x < 640 and enemy_position.x > -80) and (enemy_position.y < 360 and enemy_position.y > -45):
		enemy_position = Vector2(rand_range(-160, 670), rand_range(-90, 390))
	
	Global.instance_node(enemies[enemy_number], enemy_position, self)

func _on_Difficulty_Timer_timeout() -> void:
	if enemySpawnTimer.wait_time > 0.65:
		enemySpawnTimer.wait_time -= 0.025

func _on_Powerup_Spawn_Timer_timeout() -> void:
	var powerup_position : Vector2 = Vector2(rand_range(0, 640), rand_range(0, 360))
	var powerup_number : int = round(rand_range(0, powerups.size() - 1))
	
	Global.instance_node(powerups[powerup_number], powerup_position, self)

func _exit_tree() -> void:
	Global.node_creation_parent = null
