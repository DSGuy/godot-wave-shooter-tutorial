# Code written by Oladeji Sanyaolu (Bullet) 11/2/2022

extends Sprite

var velocity : Vector2 = Vector2.RIGHT
var speed : int = 250
var damage : int

var look_once : bool = true

onready var visibilityNotifier : VisibilityNotifier2D = get_node("VisibilityNotifier2D")
onready var hitbox : Area2D = get_node("Hitbox")

func _ready() -> void:
	visibilityNotifier.connect("screen_exited", self, "_on_VisibilityNotifier2D_screen_exited")
	hitbox.connect("area_entered", self, "_on_Hitbox_area_entered")
	hitbox.add_to_group("Enemy_Damager")
	
func _process(delta : float) -> void:
	if look_once:
		look_at(get_global_mouse_position())
		look_once = false
	
	global_position += velocity.rotated(rotation) * speed * delta
	pass

func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()
	pass

func _on_Hitbox_area_entered(area : Area2D) -> void:
	pass
